//
//  ViewController.swift
//  PharmaTutor
//
//  Created by Thumar Kishan on 06/07/21.
//

import UIKit
import CarbonKit

class ViewController: UIViewController, CarbonTabSwipeNavigationDelegate
{
       
    
    @IBOutlet weak var Btn_Sidemenuu: UIBarButtonItem!
    
    var tabSwipe:CarbonTabSwipeNavigation!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.white
        ]
        navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationItem.setHidesBackButton(true, animated: true)
    
        
        NotificationCenter.default.addObserver(self, selector: #selector(loadList), name: NSNotification.Name(rawValue: "Sidemenu_Select"), object: nil)

        
        let gradient = CAGradientLayer()
        let sizeLength = UIScreen.main.bounds.size.height * 2
        let defaultNavigationBarFrame = CGRect(x: 0, y: 0, width: sizeLength, height: 64)
        gradient.frame = defaultNavigationBarFrame
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        gradient.colors = [UIColor(red: 0.05, green: 0.75, blue: 0.49, alpha: 1).cgColor, UIColor(red: 0.03, green: 0.85, blue: 0.68, alpha: 0.25).cgColor]
        navigationController?.navigationBar.setBackgroundImage(self.image(fromLayer: gradient), for: .default)

        tabSwipe = CarbonTabSwipeNavigation(items: ["ARTICLE", "NEWS", "MAGAZINE", "JOBS", "JOB CATEGORY", "COURSES", "CITY WISE ALERTS"], delegate: self)
        tabSwipe.setTabExtraWidth(10)
        tabSwipe.setNormalColor((UIColor .white.withAlphaComponent(0.6)), font: UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.semibold))
        tabSwipe.setSelectedColor((UIColor .white.withAlphaComponent(1.0)), font: UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.bold))
        tabSwipe.setIndicatorColor(UIColor(red: 0.05, green: 0.76, blue: 0.52, alpha: 1.00))

        tabSwipe.toolbar.barTintColor = (UIColor(red: 0.05, green: 0.76, blue: 0.52, alpha: 1.00))
        tabSwipe.insert(intoRootViewController: self)
//        tabSwipe.setCurrentTabIndex(5, withAnimation:true)


//        UserDefaults.standard.set(0, forKey: "Sidemenu_Select")  //Integer

        
        Btn_Sidemenuu.target = revealViewController()
        Btn_Sidemenuu.action = #selector(revealViewController()?.revealSideMenu)
        self.revealViewController()?.gestureEnabled = true

    }
    
    @objc func loadList(notification: NSNotification)
    {
//        let tabSwipe = CarbonTabSwipeNavigation(items: ["ARTICLE", "NEWS", "MAGAZINE", "JOBS", "JOB CATEGORY", "COURSES", "CITY WISE ALERTS"], delegate: self)
//        tabSwipe.setTabExtraWidth(10)
//        tabSwipe.setNormalColor((UIColor .white.withAlphaComponent(0.6)), font: UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.semibold))
//        tabSwipe.setSelectedColor((UIColor .white.withAlphaComponent(1.0)), font: UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.bold))
//        tabSwipe.setIndicatorColor(UIColor(red: 0.05, green: 0.76, blue: 0.52, alpha: 1.00))
//
//        tabSwipe.toolbar.barTintColor = (UIColor(red: 0.05, green: 0.76, blue: 0.52, alpha: 1.00))

        let dataget:Int =  UserDefaults.standard.integer(forKey: "Sidemenu_Select")
        if dataget == 0
        {
            tabSwipe.setCurrentTabIndex(0, withAnimation:true)
        }
        if dataget == 1
        {
            tabSwipe.setCurrentTabIndex(1, withAnimation:true)
        }
        if dataget == 2
        {
            tabSwipe.setCurrentTabIndex(2, withAnimation:true)
        }
        if dataget == 3
        {
            tabSwipe.setCurrentTabIndex(3, withAnimation:true)
        }
        if dataget == 4
        {
            tabSwipe.setCurrentTabIndex(4, withAnimation:true)
        }
        if dataget == 5
        {
            tabSwipe.setCurrentTabIndex(5, withAnimation:true)
        }
        if dataget == 6
        {
            tabSwipe.setCurrentTabIndex(6, withAnimation: true)
        }

//        tabSwipe.insert(intoRootViewController: self)

    }

    func image(fromLayer layer: CALayer) -> UIImage
    {
        UIGraphicsBeginImageContext(layer.frame.size)

        layer.render(in: UIGraphicsGetCurrentContext()!)

        let outputImage = UIGraphicsGetImageFromCurrentImageContext()

        UIGraphicsEndImageContext()

        return outputImage!
    }


    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController
    {
        guard let storyboard = storyboard else
        {
            return UIViewController()
        }
        
        if index == 0
        {
            UserDefaults.standard.set(2758, forKey: "Key")  //Integer
            return storyboard.instantiateViewController(withIdentifier: "MagazineView")
        }
        
        if index == 1
        {
            UserDefaults.standard.set(2123, forKey: "Key")
            return storyboard.instantiateViewController(withIdentifier: "MagazineView")
        }
        
        if index == 2
        {
            UserDefaults.standard.set(3122, forKey: "Key")  //Integer
            return storyboard.instantiateViewController(withIdentifier: "JobsVC")
        }

        if index == 3
        {
            UserDefaults.standard.set(1754, forKey: "Key")  //Integer
            return storyboard.instantiateViewController(withIdentifier: "JobsVC")
        }
        
        if index == 4
        {
            UserDefaults.standard.set(1, forKey: "Key")  //Integer
            UserDefaults.standard.set(1, forKey: "Job")  //Integer
            return storyboard.instantiateViewController(withIdentifier: "JobCategoryView")
        }

        if index == 5
        {
            UserDefaults.standard.set(2, forKey: "Key")  //Integer
            UserDefaults.standard.set(2, forKey: "Job")  //Integer
            return storyboard.instantiateViewController(withIdentifier: "JobCategoryView")
        }
        
        if index == 6
        {
            UserDefaults.standard.set(3, forKey: "Key")  //Integer
            UserDefaults.standard.set(3, forKey: "Job")  //Integer
            return storyboard.instantiateViewController(withIdentifier: "JobCategoryView")
        }



        return storyboard.instantiateViewController(withIdentifier: "JobCategoryView")
    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, willMoveAt index: UInt)
    {
        if index == 0
        {
            UserDefaults.standard.set(2758, forKey: "Key")  //Integer
        }
        
        if index == 1
        {
            UserDefaults.standard.set(2123, forKey: "Key")
        }
        
        if index == 2
        {
            UserDefaults.standard.set(3122, forKey: "Key")  //Integer
        }

        if index == 3
        {
            UserDefaults.standard.set(1754, forKey: "Key")  //Integer
        }
        
        if index == 4
        {
            UserDefaults.standard.set(1, forKey: "Key")  //Integer
            UserDefaults.standard.set(1, forKey: "Job")  //Integer

        }

        if index == 5
        {
            UserDefaults.standard.set(2, forKey: "Key")  //Integer
            UserDefaults.standard.set(2, forKey: "Job")  //Integer
        }
        
        if index == 6
        {
            UserDefaults.standard.set(3, forKey: "Key")  //Integer
            UserDefaults.standard.set(3, forKey: "Job")  //Integer
        }

    }

}

