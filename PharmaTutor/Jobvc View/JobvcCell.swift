//
//  JobvcCell.swift
//  PharmaTutor
//
//  Created by Thumar Kishan on 07/07/21.
//

import UIKit

class JobvcCell: UITableViewCell
{
    @IBOutlet weak var Lbl_Jobdata: UILabel!
    @IBOutlet weak var Lbl_JobTime: UILabel!
    
    @IBOutlet weak var Btn_Share: UIButton!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }

}
