//
//  GetDetailData.swift
//  PharmaTutor
//
//  Created by Thumar Kishan on 07/08/21.
//

import Foundation
import Alamofire

extension APIManager
{
    func GetDetailData( parameters:NSDictionary, onSuccess: @escaping(NSDictionary) -> Void, onFailure: @escaping(Error) -> Void)
    {

        let str_node_id : String = parameters["node_id"] as! String // accesses the value of key "One"

        
        let parameters = ["node_id": str_node_id]

        print(parameters)
        
        
        Alamofire.request(String(format: "%@", UrlGetDetailData), method: .post, parameters: parameters, encoding: URLEncoding.httpBody, headers: nil).responseJSON { (response) in
           // print(response)
            
            switch(response.result)
            {
                
            case .success( _):
                
                do {
                    let json = try JSONSerialization.jsonObject(  with: response.data!, options:.allowFragments)
                    if let json = json as? NSDictionary{
                        if let data = json.value(forKey:"data") as? NSArray{
                            print(data)
                        }else{
                            
                        }
                        print(json)
                        
                        onSuccess(json)
                    }else{
                        print(json as! NSDictionary)
                        
                        onSuccess(json as! NSDictionary)
                    }
                   
                } catch let myJSONError {
                    print()
                      onFailure(myJSONError)
                }
            
            case .failure(let error):
                print(error)
               onFailure(error)
                break;
            }
        }
    }
    }
