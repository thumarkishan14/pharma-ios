//
//  GetData.swift
//  PharmaTutor
//
//  Created by Thumar Kishan on 09/07/21.
//

import Foundation
import Alamofire

extension APIManager
{
    func GetRandomImageVideo( parameters:NSDictionary, onSuccess: @escaping(NSDictionary) -> Void, onFailure: @escaping(Error) -> Void)
    {
        let str_termid : String = parameters["term_id"] as! String // accesses the value of key "One"
        let str_pageid : String = parameters["page"] as! String// accesses the value of key "One"

        
        let parameters = ["term_id": str_termid, "page": str_pageid]

        print(parameters)
        
        
        Alamofire.request(String(format: "%@", urlApp), method: .post, parameters: parameters, encoding: URLEncoding.httpBody, headers: nil).responseJSON { (response) in
           // print(response)
            
            switch(response.result)
            {
                
            case .success( _):
                
                do {
                    let json = try JSONSerialization.jsonObject(  with: response.data!, options:.allowFragments)
                    if let json = json as? NSDictionary{
                        if let data = json.value(forKey:"data") as? NSArray{
                            print(data)
                        }else{
                            
                        }
                        print(json)
                        
                        onSuccess(json)
                    }else{
                        print(json as! NSDictionary)
                        
                        onSuccess(json as! NSDictionary)
                    }
                   
                } catch let myJSONError {
                    print()
                      onFailure(myJSONError)
                }
            
            case .failure(let error):
                print(error)
               onFailure(error)
                break;
            }
        }
    }
}
