//
//  MagazineView.swift
//  PharmaTutor
//
//  Created by Thumar Kishan on 12/07/21.
//

import UIKit
import SDWebImage
import AVFoundation
import NVActivityIndicatorView
import FBAudienceNetwork

class MagazineView: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, FBInterstitialAdDelegate
{

    @IBOutlet weak var Collectionview_Magazine: UICollectionView!
    var str_collecctionsection: String!
    
    var IdArray:NSMutableArray = NSMutableArray()
    var TitleArray:NSMutableArray = NSMutableArray()
    var DateArray:NSMutableArray = NSMutableArray()
    var ImageArray:NSMutableArray = NSMutableArray()
    var BodyArray:NSMutableArray = NSMutableArray()

    var str_pagination: String!

    var IdArray111:NSMutableArray = NSMutableArray()
    var TitleArray111:NSMutableArray = NSMutableArray()
    var DateArray111:NSMutableArray = NSMutableArray()
    var ImageArray111:NSMutableArray = NSMutableArray()
    var BodyArray111:NSMutableArray = NSMutableArray()

    var ad_count:Int!
    var IndexpathRow_count:Int!
    var Str_AD_CheckLoad = ""
    private var interstitialAd: FBInterstitialAd?

    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        Str_AD_CheckLoad = "2"
        let interstitialAd = FBInterstitialAd(placementID: "YOUR_PLACEMENT_ID")
        interstitialAd.delegate = self
        interstitialAd.load()
        self.interstitialAd = interstitialAd

        Collectionview_Magazine.delegate = self
        Collectionview_Magazine.dataSource = self
        

        
        let str_dataterm:Int =  UserDefaults.standard.integer(forKey: "Key")
        let str_datetimekeyaa: String! = String(format:"%@_time",String(str_dataterm))
        let str_datetimecheck: String! =  UserDefaults.standard.string(forKey: str_datetimekeyaa)

        if str_datetimecheck == nil
        {
            str_collecctionsection="1"
            str_pagination = "1"
            GetMagazine_Data()
        }
        else
        {
            str_collecctionsection="1"

            let defaults1 = UserDefaults.standard
            let str_nid1: String! = String(format:"%@_nid",String(str_dataterm))
            let str_title1: String! = String(format:"%@_title",String(str_dataterm))
            let str_date1: String! = String(format:"%@_date",String(str_dataterm))
            let str_image1: String! = String(format:"%@_image",String(str_dataterm))
            let str_body1: String! = String(format:"%@_body",String(str_dataterm))

            let IDstrings = defaults1.object(forKey: str_nid1)
            let Titlestrings = defaults1.object(forKey: str_title1)
            let Datestrings = defaults1.object(forKey: str_date1)
            let imagestrings = defaults1.object(forKey: str_image1)
            let bodystrings = defaults1.object(forKey: str_body1)

            self.IdArray111 = (IDstrings as! NSArray).mutableCopy() as! NSMutableArray
            self.TitleArray111 = (Titlestrings as! NSArray).mutableCopy() as! NSMutableArray
            self.DateArray111 = (Datestrings as! NSArray).mutableCopy() as! NSMutableArray
            self.ImageArray111 = (imagestrings as! NSArray).mutableCopy() as! NSMutableArray
            self.BodyArray111 = (bodystrings as! NSArray).mutableCopy() as! NSMutableArray

//            self.TitleArray111 = Titlestrings as! NSMutableArray
//            self.DateArray111 = Datestrings as! NSMutableArray

            for i in 0..<self.IdArray111.count
            {
                self.IdArray.add(self.IdArray111[i] as! String)
                self.TitleArray.add(self.TitleArray111[i] as! String)
                self.DateArray.add(self.DateArray111[i] as! String)
                self.ImageArray.add(self.ImageArray111[i] as! String)
                self.BodyArray.add(self.BodyArray111[i] as! String)

            }
            let str_paginationkey1: String! = String(format:"%@_page",String(str_dataterm))
            str_pagination =  UserDefaults.standard.string(forKey:str_paginationkey1)
            print(str_pagination as Any)
            Collectionview_Magazine.reloadData()
            
        }
        

    }
    
    func interstitialAdDidLoad(_ interstitialAd: FBInterstitialAd)
    {
      guard interstitialAd.isAdValid else {
        return
      }
        Str_AD_CheckLoad = "1"
        
        
      print("Ad is loaded and ready to be displayed")
    }

    func interstitialAdWillLogImpression(_ interstitialAd: FBInterstitialAd) {
      print("The user sees the ad")
      // Use this function as indication for a user's impression on the ad.
    }

    func interstitialAdDidClick(_ interstitialAd: FBInterstitialAd) {
      print("The user clicked on the ad and will be taken to its destination")
      // Use this function as indication for a user's click on the ad.
    }

    func interstitialAdWillClose(_ interstitialAd: FBInterstitialAd) {
      print("The user clicked on the close button, the ad is just about to close")
      // Consider to add code here to resume your app's flow
    }

    func interstitialAdDidClose(_ interstitialAd: FBInterstitialAd)
    {
      print("The user clicked on the close button, the ad is just about to close")
        let detail = storyboard?.instantiateViewController(withIdentifier: "DetailView") as! DetailView
        detail.Str_ImageDetail = (ImageArray[IndexpathRow_count] as! String)
        detail.Str_TitleDetail = (TitleArray[IndexpathRow_count] as! String)
        detail.Str_BodyDetail = (BodyArray[IndexpathRow_count] as! String)
        self.navigationController?.pushViewController(detail, animated: true)
    }

    func interstitialAd(_ interstitialAd: FBInterstitialAd, didFailWithError error: Error) {
      print("Interstitial ad failed to load with error: \(error.localizedDescription)")

    }

    //MARK: UICollectionViewDataSource
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 2
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if (section==1)
        {
            return TitleArray.count-1
        }
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        print(indexPath.row)
        if indexPath.section == 0
        {
            let cell = collectionView.dequeueReusableCell(
                withReuseIdentifier: "TrendingCollectionCell",
                for: indexPath) as! TrendingCollectionCell
            
            if TitleArray.count == 0
            {
            }
            else
            {
                cell.Lbl_Trending.text = (TitleArray[indexPath.row] as! String)

                cell.Imageview_Trending.sd_imageIndicator = SDWebImageActivityIndicator.gray
                let img_url = ImageArray[indexPath.row] as! String
                cell.Imageview_Trending.sd_setImage(with: URL(string: img_url), placeholderImage: nil)
            }
            


//            cell.Imageview_Trending.layer.cornerRadius = 8.0
//            cell.Imageview_Trending.layer.masksToBounds = true

            str_collecctionsection = "2"
            
            return cell

        }
        else
        {
            let cell = collectionView.dequeueReusableCell(
                withReuseIdentifier: "TodaysReadCollectionCell",
                for: indexPath) as! TodaysReadCollectionCell

            if TitleArray.count == 0
            {
            }
            else
            {
                cell.Lbl_TodayRead.text = (TitleArray[indexPath.row+1] as! String)

                cell.Imageview_Todayread.sd_imageIndicator = SDWebImageActivityIndicator.gray
                let img_url = ImageArray[indexPath.row+1] as! String
                cell.Imageview_Todayread.sd_setImage(with: URL(string: img_url), placeholderImage: nil)
            }

            if indexPath.row == self.TitleArray.count - 2
            {
                let a:Int? = Int(str_pagination)
                let b:Int? = a!+1
                str_pagination = String(b!)

                GetMagazine_Data()
            }


            cell.Imageview_Todayread.layer.cornerRadius = 20.0
            cell.Imageview_Todayread.layer.masksToBounds = true

            return cell
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let padding: CGFloat =  10
        let collectionViewSize = Collectionview_Magazine.frame.size.width - padding
        
        if indexPath.section == 0
        {
            return CGSize(width: Collectionview_Magazine.frame.size.width, height: 270)
        }
        
          return CGSize(width: collectionViewSize/2, height: collectionViewSize/2)
      }


    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize
    {
        return CGSize(width: collectionView.frame.width, height: 0)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let defaults1 = UserDefaults.standard
        let Str_AD_Check = defaults1.object(forKey: "ad_check")as! Int
        if Str_AD_Check <= 5
        {
            IndexpathRow_count = indexPath.row
            
            let defaults1 = UserDefaults.standard
            let Str_AD_Check = defaults1.object(forKey: "ad_check")as! Int
            ad_count = Str_AD_Check + 1
            UserDefaults.standard.set(ad_count, forKey: "ad_check")
            interstitialAd?.show(fromRootViewController: self)
        }
        else
        {
            let detail = storyboard?.instantiateViewController(withIdentifier: "DetailView") as! DetailView
            detail.Str_ImageDetail = (ImageArray[indexPath.row] as! String)
            detail.Str_TitleDetail = (TitleArray[indexPath.row] as! String)
            detail.Str_BodyDetail = (BodyArray[indexPath.row] as! String)
            self.navigationController?.pushViewController(detail, animated: true)
        }
    }
    
    private func GetMagazine_Data()
    {
        let dataget:Int =  UserDefaults.standard.integer(forKey: "Key")
        var str = ""
        if dataget == 2758
        {
             str = "2758"
        }
        else if dataget == 2123
        {
            str = "2123"
        }

        let parameters = ["term_id": str, "page": str_pagination]

        let frame = CGRect(x: (self.view.bounds.width/2) - 25, y: (self.view.bounds.height/2) - 108, width: 50, height: 50)
        let activityIndicatorView = NVActivityIndicatorView(frame: frame,
                                                            type: NVActivityIndicatorType.ballSpinFadeLoader, color: UIColor.black)
        self.view.addSubview(activityIndicatorView)
        activityIndicatorView.startAnimating()

        APIManager.shared.GetRandomImageVideo( parameters: parameters as NSDictionary, onSuccess: { [self] (data) in

           guard (data.value(forKey:"success") as? NSNumber) != nil else
           {
            if let dataArray = data.value(forKey: "data") as? NSArray
            {
                print(data)
                for dic in dataArray
                {
                    let dictionary = dic as! NSDictionary
                    self.fillData_Alldata(dic:dictionary)
                }
                
                let str_termiddd = String(str)

                let defaults = UserDefaults.standard
                let str_nid: String! = String(format:"%@_nid",str_termiddd)
                let str_title: String! = String(format:"%@_title",str_termiddd)
                let str_date: String! = String(format:"%@_date",str_termiddd)
                let str_image: String! = String(format:"%@_image",str_termiddd)
                let str_body: String! = String(format:"%@_body",str_termiddd)

                let str_paginationkey: String! = String(format:"%@_page",str_termiddd)
                
                defaults.set(self.IdArray, forKey:str_nid)
                defaults.set(self.TitleArray, forKey:str_title)
                defaults.set(self.DateArray, forKey:str_date)
                defaults.set(self.ImageArray, forKey:str_image)
                defaults.set(self.BodyArray, forKey:str_body)

                UserDefaults.standard.set(self.str_pagination, forKey: str_paginationkey)

                
                let str_datetimekey: String! = String(format:"%@_time",str_termiddd)
                let date = Date()
                let df = DateFormatter()
                df.dateFormat = "yyyy-MM-dd HH:mm:ss"
                let dateString = df.string(from: date)
                UserDefaults.standard.set(dateString, forKey: str_datetimekey)

                self.Collectionview_Magazine.reloadData()
                activityIndicatorView.stopAnimating()

            }
            activityIndicatorView.stopAnimating()
                return
            }

        }) { (error) in
            activityIndicatorView.stopAnimating()
        }
    }

    func fillData_Alldata(dic:NSDictionary)
    {
        self.IdArray.add(dic.value(forKey:"nid") as! String)
        self.TitleArray.add(dic.value(forKey:"title") as! String)
        self.DateArray.add(dic.value(forKey:"node_date") as! String)
        self.ImageArray.add(dic.value(forKey:"image") as! String)
        self.BodyArray.add(dic.value(forKey:"body_value") as! String)
    }

}

