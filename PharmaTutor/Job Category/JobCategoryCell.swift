//
//  JobCategoryCell.swift
//  PharmaTutor
//
//  Created by Thumar Kishan on 07/07/21.
//

import UIKit

class JobCategoryCell: UITableViewCell
{

    @IBOutlet weak var Lbl_JobCategory: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
