//
//  JobCategoryView.swift
//  PharmaTutor
//
//  Created by Thumar Kishan on 07/07/21.
//

import UIKit
import FBAudienceNetwork

class JobCategoryView: UIViewController, UITableViewDelegate, UITableViewDataSource, FBInterstitialAdDelegate
{

    var JobCategory_Arr:NSMutableArray = NSMutableArray()
    var JobCourses_Arr:NSMutableArray = NSMutableArray()
    var Citywise_Arr:NSMutableArray = NSMutableArray()
   
    var JobCategoryCode_Arr:NSMutableArray = NSMutableArray()
    var JobCoursesCode_Arr:NSMutableArray = NSMutableArray()
    var CitywiseCode_Arr:NSMutableArray = NSMutableArray()

    var ad_count:Int!
    var IndexpathRow_count:Int!
    var Str_AD_CheckLoad = ""
    private var interstitialAd: FBInterstitialAd?

    override func viewDidLoad()
    {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        Str_AD_CheckLoad = "2"
        let interstitialAd = FBInterstitialAd(placementID: "YOUR_PLACEMENT_ID")
        interstitialAd.delegate = self
        interstitialAd.load()
        self.interstitialAd = interstitialAd

        
        let dataget:Int =  UserDefaults.standard.integer(forKey: "Job")


        if dataget == 1
        {
            self.JobCategory_Arr.add("Production Jobs")
            self.JobCategory_Arr.add("R&D Jobs")
            self.JobCategory_Arr.add("F&D Jobs")
            self.JobCategory_Arr.add("Sales & Marketing")
            self.JobCategory_Arr.add("QA Jobs")
            self.JobCategory_Arr.add("QC Jobs")
            self.JobCategory_Arr.add("Faculty Jobs")
            self.JobCategory_Arr.add("CRO Jobs")
            self.JobCategory_Arr.add("DRA Jobs")
            self.JobCategory_Arr.add("IPR Jobs")
            self.JobCategory_Arr.add("IT Jobs")
            self.JobCategory_Arr.add("Packaging Alerts")
            self.JobCategory_Arr.add("Hospital Pharmacist")
            self.JobCategory_Arr.add("Walk-in Jobs")
            self.JobCategory_Arr.add("Government Jobs")
            
            self.JobCategoryCode_Arr.add("1770")
            self.JobCategoryCode_Arr.add("1884")
            self.JobCategoryCode_Arr.add("1998")
            self.JobCategoryCode_Arr.add("1888")
            self.JobCategoryCode_Arr.add("1869")
            self.JobCategoryCode_Arr.add("1764")
            self.JobCategoryCode_Arr.add("2172")
            self.JobCategoryCode_Arr.add("2109")
            self.JobCategoryCode_Arr.add("2005")
            self.JobCategoryCode_Arr.add("1841")
            self.JobCategoryCode_Arr.add("2238")
            self.JobCategoryCode_Arr.add("2249")
            self.JobCategoryCode_Arr.add("1912")
            self.JobCategoryCode_Arr.add("2513")
            self.JobCategoryCode_Arr.add("2558")
            
        }
        else if dataget == 2
        {
            self.JobCourses_Arr.add("D.Pharm Alerts")
            self.JobCourses_Arr.add("B.Pharm Alerts")
            self.JobCourses_Arr.add("B.Sc Alerts")
            self.JobCourses_Arr.add("M.Pharm Alerts")
            self.JobCourses_Arr.add("M.Sc Alerts")
            self.JobCourses_Arr.add("MBA Alerts")
            self.JobCourses_Arr.add("Pharm.D Alerts")
            self.JobCourses_Arr.add("Ph.D Alerts")
            self.JobCourses_Arr.add("MBBS Alerts")
            self.JobCourses_Arr.add("MD Alerts")
            self.JobCourses_Arr.add("MS Alerts")
            
            self.JobCoursesCode_Arr.add("1862")
            self.JobCoursesCode_Arr.add("1757")
            self.JobCoursesCode_Arr.add("1758")
            self.JobCoursesCode_Arr.add("1767")
            self.JobCoursesCode_Arr.add("1768")
            self.JobCoursesCode_Arr.add("1759")
            self.JobCoursesCode_Arr.add("2345")
            self.JobCoursesCode_Arr.add("1827")
            self.JobCoursesCode_Arr.add("1780")
            self.JobCoursesCode_Arr.add("1784")
            self.JobCoursesCode_Arr.add("1783")

        }
        else if dataget == 3
        {
            self.Citywise_Arr.add("Amritsar Alerts")
            self.Citywise_Arr.add("Aurangabad Alerts")
            self.Citywise_Arr.add("Bhopal Alerts")
            self.Citywise_Arr.add("Bhubaneshwar Alerts")
            self.Citywise_Arr.add("Cochin Alerts")
            self.Citywise_Arr.add("Delhi Alerts")
            self.Citywise_Arr.add("Lucknow Alerts")
            self.Citywise_Arr.add("Vadodara Alerts")
            self.Citywise_Arr.add("Vijayawada Alerts")
            self.Citywise_Arr.add("Gurgaon Alerts")
            self.Citywise_Arr.add("Indore Alerts")
            self.Citywise_Arr.add("Ludhiana Alerts")
            self.Citywise_Arr.add("Noida Alerts")
            self.Citywise_Arr.add("Surat Alerts")
            self.Citywise_Arr.add("Vapi Alerts")
            self.Citywise_Arr.add("Visakhapatnam Alerts")
            
            self.CitywiseCode_Arr.add("2325")
            self.CitywiseCode_Arr.add("1818")
            self.CitywiseCode_Arr.add("1843")
            self.CitywiseCode_Arr.add("2257")
            self.CitywiseCode_Arr.add("2508")
            self.CitywiseCode_Arr.add("3785")
            self.CitywiseCode_Arr.add("1848")
            self.CitywiseCode_Arr.add("2382")
            self.CitywiseCode_Arr.add("1835")
            self.CitywiseCode_Arr.add("1904")
            self.CitywiseCode_Arr.add("1826")
            self.CitywiseCode_Arr.add("2255")
            self.CitywiseCode_Arr.add("1800")
            self.CitywiseCode_Arr.add("2137")
            self.CitywiseCode_Arr.add("2469")
            self.CitywiseCode_Arr.add("1769")
        }
    }

    func interstitialAdDidLoad(_ interstitialAd: FBInterstitialAd)
    {
      guard interstitialAd.isAdValid else {
        return
      }
        Str_AD_CheckLoad = "1"
        
      print("Ad is loaded and ready to be displayed")
    }

    func interstitialAdWillLogImpression(_ interstitialAd: FBInterstitialAd) {
      print("The user sees the ad")
      // Use this function as indication for a user's impression on the ad.
    }

    func interstitialAdDidClick(_ interstitialAd: FBInterstitialAd) {
      print("The user clicked on the ad and will be taken to its destination")
      // Use this function as indication for a user's click on the ad.
    }

    func interstitialAdWillClose(_ interstitialAd: FBInterstitialAd) {
      print("The user clicked on the close button, the ad is just about to close")
      // Consider to add code here to resume your app's flow
    }

    func interstitialAdDidClose(_ interstitialAd: FBInterstitialAd)
    {
      print("The user clicked on the close button, the ad is just about to close")
        let dataget:Int =  UserDefaults.standard.integer(forKey: "Job")
        if dataget == 1
        {
            let myInt1 = Int((JobCategoryCode_Arr[IndexpathRow_count] as! String))
            UserDefaults.standard.set(myInt1, forKey: "Key")  //Integer
            let detail = storyboard?.instantiateViewController(withIdentifier: "JobsVC") as! JobsVC
            detail.Str_TitleName = (JobCategory_Arr[IndexpathRow_count] as! String)
            self.navigationController?.pushViewController(detail, animated: true)
        }
        else if dataget == 2
        {
            let myInt1 = Int((JobCoursesCode_Arr[IndexpathRow_count] as! String))
            UserDefaults.standard.set(myInt1, forKey: "Key")  //Integer
            let detail = storyboard?.instantiateViewController(withIdentifier: "JobsVC") as! JobsVC
            detail.Str_TitleName = (JobCourses_Arr[IndexpathRow_count] as! String)
            self.navigationController?.pushViewController(detail, animated: true)
        }
        else if dataget == 3
        {
            let myInt1 = Int((CitywiseCode_Arr[IndexpathRow_count] as! String))
            UserDefaults.standard.set(myInt1, forKey: "Key")  //Integer
            let detail = storyboard?.instantiateViewController(withIdentifier: "JobsVC") as! JobsVC
            detail.Str_TitleName = (Citywise_Arr[IndexpathRow_count] as! String)
            self.navigationController?.pushViewController(detail, animated: true)
        }
    }

    func interstitialAd(_ interstitialAd: FBInterstitialAd, didFailWithError error: Error) {
      print("Interstitial ad failed to load with error: \(error.localizedDescription)")
        
    }


    //MARK: UITableviewDelegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        let dataget:Int =  UserDefaults.standard.integer(forKey: "Key")

        if dataget == 1
        {
            return JobCategory_Arr.count;
        }
        else if dataget == 2
        {
            return JobCourses_Arr.count;
        }
        else if dataget == 3
        {
            return Citywise_Arr.count;
        }
        return 0;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:JobCategoryCell = tableView.dequeueReusableCell(withIdentifier: "JobCategoryCell") as! JobCategoryCell
        
        let dataget:Int =  UserDefaults.standard.integer(forKey: "Job")

        if dataget == 1
        {
            cell.Lbl_JobCategory.text = (JobCategory_Arr[indexPath.row] as! String)
        }
        else if dataget == 2
        {
            cell.Lbl_JobCategory.text = (JobCourses_Arr[indexPath.row] as! String)
        }
        else if dataget == 3
        {
            cell.Lbl_JobCategory.text = (Citywise_Arr[indexPath.row] as! String)
        }

        cell.selectionStyle = .none

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let defaults1 = UserDefaults.standard
        let Str_AD_Check = defaults1.object(forKey: "ad_check")as! Int
        if Str_AD_Check <= 5
        {
            IndexpathRow_count = indexPath.row
            
            let defaults1 = UserDefaults.standard
            let Str_AD_Check = defaults1.object(forKey: "ad_check")as! Int
            ad_count = Str_AD_Check + 1
            UserDefaults.standard.set(ad_count, forKey: "ad_check")
            interstitialAd?.show(fromRootViewController: self)
        }
        else
        {
            let dataget:Int =  UserDefaults.standard.integer(forKey: "Job")
            if dataget == 1
            {
                let myInt1 = Int((JobCategoryCode_Arr[indexPath.row] as! String))
                UserDefaults.standard.set(myInt1, forKey: "Key")  //Integer
                let detail = storyboard?.instantiateViewController(withIdentifier: "JobsVC") as! JobsVC
                detail.Str_TitleName = (JobCategory_Arr[indexPath.row] as! String)
                self.navigationController?.pushViewController(detail, animated: true)
            }
            else if dataget == 2
            {
                let myInt1 = Int((JobCoursesCode_Arr[indexPath.row] as! String))
                UserDefaults.standard.set(myInt1, forKey: "Key")  //Integer
                let detail = storyboard?.instantiateViewController(withIdentifier: "JobsVC") as! JobsVC
                detail.Str_TitleName = (JobCourses_Arr[indexPath.row] as! String)
                self.navigationController?.pushViewController(detail, animated: true)
            }
            else if dataget == 3
            {
                let myInt1 = Int((CitywiseCode_Arr[indexPath.row] as! String))
                UserDefaults.standard.set(myInt1, forKey: "Key")  //Integer
                let detail = storyboard?.instantiateViewController(withIdentifier: "JobsVC") as! JobsVC
                detail.Str_TitleName = (Citywise_Arr[indexPath.row] as! String)
                self.navigationController?.pushViewController(detail, animated: true)
            }
        }
        
        
    }

}
