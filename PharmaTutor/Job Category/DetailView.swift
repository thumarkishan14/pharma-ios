//
//  DetailView.swift
//  PharmaTutor
//
//  Created by Thumar Kishan on 16/07/21.
//

import UIKit
import SDWebImage
import AVFoundation
import NVActivityIndicatorView

class DetailView: UIViewController
{
    @IBOutlet weak var Imageview_Detail: UIImageView!
    
    @IBOutlet weak var Srollview: UIScrollView!
    
    @IBOutlet weak var Lbl_Title: UILabel!
    @IBOutlet weak var Lbl_Body: UILabel!
    
    var Str_ImageDetail = ""
    var Str_TitleDetail = ""
    var Str_BodyDetail = ""
    var Str_NodeID = ""
    var Str_CheckMagazine = ""

    var Str_Data = ""
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.white
        ]
        
        UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.clear], for: .normal)
        UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.clear], for: UIControl.State.highlighted)

        navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationItem.setHidesBackButton(false, animated: true)
        self.navigationItem.title = "Detail"

        
        if Str_CheckMagazine == "magazine"
        {
            GetDetail_Data()
        }
        else
        {
            Lbl_Title.text = Str_TitleDetail
            Lbl_Body.text = Str_BodyDetail

            Imageview_Detail.sd_imageIndicator = SDWebImageActivityIndicator.gray
            Imageview_Detail.sd_setImage(with: URL(string: Str_ImageDetail), placeholderImage: nil)
        }


        let contentRect: CGRect = Srollview.subviews.reduce(into: .zero) { rect, view in
            rect = rect.union(view.frame)
        }
        Srollview.contentSize = contentRect.size

    }
    
    private func GetDetail_Data()
    {
        let parameters = ["node_id": Str_NodeID]

        let frame = CGRect(x: (self.view.bounds.width/2) - 25, y: (self.view.bounds.height/2) - 108, width: 50, height: 50)
        let activityIndicatorView = NVActivityIndicatorView(frame: frame,
                                                            type: NVActivityIndicatorType.ballSpinFadeLoader, color: UIColor.black)
        self.view.addSubview(activityIndicatorView)
        activityIndicatorView.startAnimating()

        APIManager.shared.GetDetailData( parameters: parameters as NSDictionary, onSuccess: { [self] (data) in

           guard (data.value(forKey:"success") as? NSNumber) != nil else
           {
            if let dataArray = data.value(forKey: "data") as? NSDictionary
            {
                print(dataArray)

                Lbl_Title.text = dataArray.value(forKey: "title")as? String
                let Str_body = dataArray.value(forKey: "body")as? String
                let base64Encoded = Str_body
                let decodedData = Data(base64Encoded: base64Encoded!)!
                let decodedString = String(data: decodedData, encoding: .utf8)!

                let data = decodedString.data(using: .utf8)!
                let attributedString = try? NSAttributedString(
                    data: data,
                    options: [.documentType: NSAttributedString.DocumentType.html],
                    documentAttributes: nil)

                Lbl_Body.attributedText = attributedString


//                Imageview_Detail.sd_imageIndicator = SDWebImageActivityIndicator.gray
//                Imageview_Detail.sd_setImage(with: URL(string: data.value(forKey: "tiles_h")as! String), placeholderImage: nil)
                
                activityIndicatorView.stopAnimating()

            }
            activityIndicatorView.stopAnimating()
                return
            }

        }) { (error) in
            activityIndicatorView.stopAnimating()
        }
}
    
    
}

extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return nil }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return nil
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}
